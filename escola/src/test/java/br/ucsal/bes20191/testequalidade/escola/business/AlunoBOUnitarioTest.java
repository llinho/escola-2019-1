package br.ucsal.bes20191.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Before;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;


public class AlunoBOUnitarioTest {

	private DateHelper helperMock;

	private AlunoDAO alunoDAOMock;

	private AlunoBO alunoBO;

	@Before
	public void setup() {
		helperMock = Mockito.mock(DateHelper.class);
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		alunoBO = new AlunoBO(alunoDAOMock, helperMock);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter
	 * 16 anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {

		// Entrada de dados
		Integer matricula = 90;

		// Configurando do ano atual
		Mockito.doReturn(2019).when(helperMock).obterAnoAtual();

		// Configurando o alunoDAOMock
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula)
				.nascidoEm(2003).build();
		Mockito.doReturn(aluno1).when(alunoDAOMock)
				.encontrarPorMatricula(matricula);

		// saida esperada
		Integer idadeEsperada = 16;

		// Executando do metodo
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// resultado
		Assert.assertEquals(idadeEsperada, idadeAtual);

	}

	/**
	 * Verificar se alunos ativos so atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {

		// Dados de entrada
		Aluno alunoAtivo = AlunoBuilder.umAlunoAtivo().build();

		// Executar o mtodo 
		alunoBO.atualizar(alunoAtivo);

		// Verificar o metodo
		
	     Mockito.verify(alunoDAOMock).salvar(alunoAtivo);

	}

}
