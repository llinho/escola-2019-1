package br.ucsal.bes20191.testequalidade.escola.business;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;



public class AlunoBuilder {
	
	
	private static final Integer MATRICULA_DEFAULT = 1;
	private static final String NOME_DEFAULT = "Paulo";
	private static final SituacaoAluno SITUACAO_DEFAULT = SituacaoAluno.ATIVO;
	private static final Integer ANO_NASCIMENTO_DEFAULT = 2003;

	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoAluno situacao = SITUACAO_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;

	private AlunoBuilder() {

	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	public static AlunoBuilder umAlunoAtivo() {
		return new AlunoBuilder().ativo();
	}

	public static AlunoBuilder umAlunoCancelado() {
		return new AlunoBuilder().cancelado();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder ativo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder cancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	public AlunoBuilder nascidoEm(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder mas() {
		AlunoBuilder novoAlunoBuilder = umAluno();
		novoAlunoBuilder.matricula = matricula;
		novoAlunoBuilder.nome = nome;
		novoAlunoBuilder.situacao = situacao;
		novoAlunoBuilder.anoNascimento = anoNascimento;
		return novoAlunoBuilder;
	}

	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}
	

}
