package br.ucsal.bes20191.testequalidade.escola.tui;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;



public class TuiHelperUnitarioTest {

	/**
	 * Verificar a obtenção do nome completo. Caso de teste: primeiro nome
	 * "Claudio" e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {

		TuiUtil tuiUtil = new TuiUtil();
		Scanner scannerMock = Mockito.mock(Scanner.class);
		tuiUtil.scanner = scannerMock;

		// Entrada de Dados
		String nome = "Claudio";
		String sobrenome = "Neiva";
		Mockito.doReturn(nome).doReturn(sobrenome).when(scannerMock).nextLine();

		//sa�da esperada
		String nomeCompletoEsperado = "Claudio Neiva";

		// Executar o m�todo 
		String nomeCompletoAtual = tuiUtil.obterNomeCompleto();
  
		
		Assert.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);

	}

	/**
	 * Verificar a obtenção exibição de mensagem. Caso de teste: mensagem
	 * "Tem que estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {

		TuiUtil tuiUtil = new TuiUtil();

		
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

	
		String mensagem = "Tem que estudar!";

		tuiUtil.exibirMensagem(mensagem);

		// Verificar se foi chamado"
		
		Mockito.verify(printStreamMock).println("Bom dia!");
		Mockito.verify(printStreamMock).println(mensagem);
	}

}
